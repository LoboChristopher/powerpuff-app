import { ActionType } from "../action-types/index";
import { Dispatch } from "redux";
import { Action } from "../actions/index";

export const show = (show: []) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.SHOW,
      payload: show,
    });
  };
};
