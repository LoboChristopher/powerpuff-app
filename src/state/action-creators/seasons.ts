import { ActionType } from "../action-types/seasons";
import { Dispatch } from "redux";
import { Action } from "../actions/seasons";

export const seasons = (seasons: []) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.SEASONS,
      payload: seasons,
    });
  };
};
