import { ActionType } from "../action-types/episodes";
import { Dispatch } from "redux";
import { Action } from "../actions/episodes";

export const episodes = (episodes: []) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.EPISODES,
      payload: episodes,
    });
  };
};
