import { ActionType } from "../action-types/details";
import { Dispatch } from "redux";
import { Action } from "../actions/details";

export const details = (details: []) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.DETAILS,
      payload: details,
    });
  };
};
