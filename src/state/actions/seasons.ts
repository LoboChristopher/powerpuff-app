import { ActionType } from "./../action-types/seasons";

interface SEASONS {
  type: ActionType.SEASONS;
  payload: any;
}

export type Action = SEASONS;
