import { ActionType } from "./../action-types/episodes";

interface Episodes {
  type: ActionType.EPISODES;
  payload: any;
}

export type Action = Episodes;
