import { ActionType } from "./../action-types/index";

interface Show {
  type: ActionType.SHOW;
  payload: any;
}

export type Action = Show;
