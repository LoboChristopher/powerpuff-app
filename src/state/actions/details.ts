import { ActionType } from "./../action-types/details";

interface Episodes {
  type: ActionType.DETAILS;
  payload: any;
}

export type Action = Episodes;
