import { ActionType } from "../action-types/details";
import { Action } from "../actions/details";

const initialState = {};

const reducer = (state: {} = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.DETAILS:
      return action.payload;
    default:
      return state;
  }
};

export default reducer
