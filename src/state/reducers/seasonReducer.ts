import { ActionType } from "../action-types/seasons";
import { Action } from "../actions/seasons";

const initialState = {};

const reducer = (state: {} = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.SEASONS:
      return action.payload;
    default:
      return state;
  }
};

export default reducer
