import { Action } from './../actions/episodes';
import { ActionType } from "../action-types/episodes";

const initialState = {};

const reducer = (state: {} = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.EPISODES:
      return action.payload;
    default:
      return state;
  }
};

export default reducer
