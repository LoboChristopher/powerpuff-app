import { combineReducers } from "redux";
import activityReducer from "./activityReducer";
import episodesReducer from "./episodesReducer";
import seasonsReducer from "./seasonReducer";
import detailsReducer from "./detailsReducer";

const reducers = combineReducers({
  activity: activityReducer,
  episodes: episodesReducer,
  seasons: seasonsReducer,
  details: detailsReducer,
});

export default reducers;

export type State = ReturnType<typeof reducers>;
