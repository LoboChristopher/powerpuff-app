import { ActionType } from "../action-types";
import { Action } from "../actions";

const initialState = {};

const reducer = (state: {} = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.SHOW:
      return action.payload;
    default:
      return state;
  }
};

export default reducer
