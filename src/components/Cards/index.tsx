import React, { FC } from "react";
import { Link } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

import { CardTypes } from "./types";

import { Container } from "./styles";

const Cards: FC<CardTypes> = ({
  episodeNumber,
  name,
  episodeId,
  image,
  color,
}) => {
  return (
    <Container>
      <Card
        sx={{
          maxWidth: 345,
          minWidth: 345,
          backgroundColor: color,
        }}
      >
        <Link to={`/Details/`+episodeId+`/`+color} component={CardActionArea}>
          <CardMedia
            component="img"
            height="140"
            image={image}
            alt="green iguana"
          />
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              sx={{ color: "white" }}
            >
              Episode: {episodeNumber}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ color: "white" }}
            >
              {name}
            </Typography>
          </CardContent>
        </Link>
      </Card>
    </Container>
  );
};

export default Cards;
