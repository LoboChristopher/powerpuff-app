export type CardTypes = {
    episodeNumber: number
    season?: number
    name: string
    link: string
    episodeId: string
    image: string
    color: string
}
