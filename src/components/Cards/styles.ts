import styled, { css } from "styled-components";

export const Container = styled.div`
  min-height: 20vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: 2.5rem;
  color:  ${({ theme }) => theme.colors.white};

  a{
    color: ${({ theme }) => theme.colors.white};
  }
`;
