import 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    title: string

    colors: {
      primary: string
      secondary: string
      white: string
      black: string
      blossomPink: string
      bubblesBlue: string
      buttercupGreen: string
      powerpuffLogoPink: string
    }

    fonts: {
      primary: string
      sizes: {
        headerLinks: string
        headerMobileLinks: string
        sectionName: string
        brandsTitle: string
        heroTitle: string
        title: string
        intro: string
        button: string
        news: {
          title: string
          links: string
        }
        sectionTitle: string
        sectionIntro: string
        blockTitle: string
      }
    }

    wrapper: {
      width: string
      maxWidth: string
    }

  }
}
