const light = {
  title: 'light',

  colors: {
    primary: '#ff5b21',
    secondary: '#111820',
    white: '#FFFFFF',
    black: '#000000',
    blossomPink: '#E87BAB',
    bubblesBlue: '#3EA6CB',
    buttercupGreen: '#57AF23',
    powerpuffLogoPink: '#F472B5'
  },


  fonts: {
    primary: 'PowerPuff Girls',
    sizes: {
      headerLinks: '.9rem',
      headerMobileLinks: '1.2rem',
      sectionName: '.75rem',
      brandsTitle: '1.4rem',
      heroTitle: '3.5rem',
      title: '2.2rem',
      intro: '1rem',
      button: '.9rem',
      news: {
        title: '1.5rem',
        links: '.9rem',
      },
      sectionTitle: '2.5rem',
      sectionIntro: '1.25rem',
      blockTitle: '2rem',
    },
  },

  wrapper: {
    width: '1240px',
    maxWidth: '90%',
  },
}

export default light
