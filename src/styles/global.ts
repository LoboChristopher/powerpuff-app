import { createGlobalStyle } from "styled-components";
import PowerPuffFontTTF from "../assets/fonts/powerpuff_girls_font.ttf";

const GlobalStyle = createGlobalStyle`
@font-face {
  font-family: 'PowerPuff Girls';
  src: url(${PowerPuffFontTTF}) format('ttf'),
}
  body {
    font-family: 'PowerPuff Girls', sans-serif;
    background-color: ${({ theme }) => theme.colors.black};
  }
`;

export default GlobalStyle;
