import IRoute from "../interfaces/route";
import Details from "../views/Details";
import Show from "../views/Show";

const routes: IRoute[] = [
    {
        path: "/",
        name: "Show Page",
        component: Show,
        exact: true,
      },
      {
        path: "/details/:episodeId",
        name: "Details Page",
        component: Details,
        exact: true,
      },
];

export default routes;
