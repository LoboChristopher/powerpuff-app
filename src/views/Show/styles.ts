import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  font-family: ${({ theme }) => theme.fonts.primary};

`;

export const ShowContainer = styled.div`
  display: flex;
  margin-bottom: 4rem;
  @media(max-width: 800px) { 
    flex-direction: column;
  }

  @media(max-width: 1024px) { 
    flex-direction: column;
  }

  @media(max-width: 1280px) { 
    flex-direction: column;
  }
`;

export const ShowImageContainer = styled.div`
text-align: center;
`

export const ShowImage = styled.img`
  width: 20rem;
  border-style: solid;
`;

export const ShowContent = styled.div`
  margin-left: 2rem;
`;

export const ShowSummary = styled.div`
  p{
    font-size: 20px;  
    line-height: 40px;
  }
`;

export const ShowName = styled.p`
  color: ${({ theme }) => theme.colors.powerpuffLogoPink};
  font-size: 40px;
  font-weight: 1000;
`;

export const ContentContainer = styled.div`
  background-color: ${({ theme }) => theme.colors.black};
  color: ${({ theme }) => theme.colors.white};
  width: 80%;
  margin-left: 5rem;
  @media(max-width: 800px) { 
    width: 65%;
  }
  @media(max-width: 1024px) { 
    width: 65%;
  }

  @media(max-width: 1280px) { 
    width: 65%;
  }
`;

export const SeasonContainer = styled.div`
  background-color: ${({ theme }) => theme.colors.black};
  margin-top: 0.5rem;
  &:nth-child(1n + 1) {
    background-color: ${({ theme }) => theme.colors.blossomPink};
  }
  &:nth-child(2n + 1) {
    background-color: ${({ theme }) => theme.colors.bubblesBlue};
  }
  &:nth-child(3n + 1) {
    background-color: ${({ theme }) => theme.colors.buttercupGreen};
  }
`;

export const EpisodeContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

export const StripescontainerLeft = styled.div``;

export const StripescontainerRight = styled.div`
  float: right;
  width: 90px;
`;

export const BlossomStripe = styled.div`
  position: fixed;
  margin-left: 30px;
  width: 30px;
  height: 100vh;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.blossomPink};
`;

export const BubblesStripe = styled.div`
  position: fixed;
  margin-left: 60px;
  width: 30px;
  height: 100vh;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.bubblesBlue};
`;

export const ButtercupStripe = styled.div`
  position: fixed;
  width: 30px;
  height: 100vh;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.buttercupGreen};
`;
