import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";

import IPage from "../../interfaces/page";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import Cards from "../../components/Cards";
import api from "../../services/api";
import {
  actionCreators,
  episodeActionCreator,
  seasonActionCreator,
  State,
} from "../../state";

import {
  Container,
  ShowContainer,
  ShowName,
  ShowImageContainer,
  ShowImage,
  ShowContent,
  ShowSummary,
  ContentContainer,
  SeasonContainer,
  EpisodeContainer,
  StripescontainerLeft,
  StripescontainerRight,
  BlossomStripe,
  BubblesStripe,
  ButtercupStripe,
} from "./styles";

const Show: React.FC<IPage> = (props) => {
  const dispatch = useDispatch();

  const { show } = bindActionCreators(actionCreators, dispatch);
  const tvShow = useSelector((state: State) => state.activity);

  const { seasons } = bindActionCreators(seasonActionCreator, dispatch);
  const showSeasons = useSelector((state: State) => state.seasons);

  const { episodes } = bindActionCreators(episodeActionCreator, dispatch);
  const showEpisodes = useSelector((state: State) => state.episodes);

  const [render, setRender] = useState(false);
  const [renderEpisodes, setRenderEpisodes] = useState(false);

  async function getShow() {
    try {
      const { data } = await api.get("shows/1955");
      show(data);
    } catch (error) {
      alert("There was a problem fetching the Show");
    }
    setRender(true);
    getSeasons();
  }

  async function getEpisodes() {
    try {
      const { data } = await api.get("shows/1955/episodes");
      episodes(data);
    } catch (error) {
      alert("There was a problem fetching the Episodes");
    }
    setRenderEpisodes(true);
  }

  async function getSeasons() {
    try {
      const { data } = await api.get("shows/1955/seasons");
      seasons(data);
    } catch (error) {
      alert("There was a problem fetching the Episodes");
    }
    getEpisodes();
  }

  useEffect(() => {
    getShow();
  }, []);

  return (
    <Container>
      <StripescontainerLeft>
        <ButtercupStripe />
        <BlossomStripe />
        <BubblesStripe />
      </StripescontainerLeft>
      <ContentContainer>
        {render === true && (
          <>
            <ShowContainer>
              <ShowImageContainer>
                <ShowImage src={tvShow?.image.original} alt={tvShow?.name} />
              </ShowImageContainer>
              <ShowContent>
                <ShowName>{tvShow?.name}</ShowName>
                <ShowSummary
                  dangerouslySetInnerHTML={{ __html: `${tvShow.summary}` }}
                />
              </ShowContent>
            </ShowContainer>
            {renderEpisodes === true &&
              showSeasons?.map((seasons: any) => (
                <SeasonContainer key={seasons.id}>
                  <Accordion>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography>Season {seasons.number}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <EpisodeContainer>
                        {showEpisodes.map(
                          (episode: any) =>
                            seasons.number === episode.season && (
                              <>
                                <Cards
                                  name={episode.name}
                                  season={episode.season}
                                  link={episode.url}
                                  episodeId={episode.id}
                                  image={episode.image.medium}
                                  episodeNumber={episode.number}
                                  color={
                                    episode.number % 2 === 0
                                      ? "#3EA6CB"
                                      : "#57AF23" &&
                                        (episode.number % 3) - 1 === 0
                                      ? "#E87BAB"
                                      : "#57AF23"
                                  }
                                />
                              </>
                            )
                        )}
                      </EpisodeContainer>
                    </AccordionDetails>
                  </Accordion>
                </SeasonContainer>
              ))}
          </>
        )}
      </ContentContainer>
      <StripescontainerRight>
        <ButtercupStripe />
        <BlossomStripe />
        <BubblesStripe />
      </StripescontainerRight>
    </Container>
  );
};

export default Show;
