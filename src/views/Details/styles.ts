import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  font-family: ${({ theme }) => theme.fonts.primary};

`;

export const ContentContainer = styled.div`
  color: ${({ theme }) => theme.colors.white};
  font-size: 20px;
  height: 98vh;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 80rem;
  padding-left: 6rem;
  @media(max-width: 800px) { 
    flex-direction: column;
    width: 85%;
  }

  @media(max-width: 1024px) { 
    flex-direction: column;
    width: 85%;
  }

  @media(max-width: 1280px) { 
    flex-direction: column;
    width: 85%;
  }
}

`;

export const DetailsContainer = styled.div`
  background-color: ${(props) => props.color};
  display: flex;
  justify-content: center;
  width: 80%;
  flex-direction: row;
  padding: 4rem;
  border-radius: 1rem;
  width: 75%;
  @media(max-width: 800px) { 
    flex-direction: column;
    width: 50%;
  }

  @media(max-width: 1024px) { 
    flex-direction: column;
    width: 50%;
  }

  @media(max-width: 1280px) { 
    flex-direction: column;
    width: 50%;
  }
`;

export const DetailsContentContainer = styled.div`
  padding: 0 4rem;
  width: 90%;
  @media(max-width: 800px) { 
    padding: 0 1rem;
    margin-bottom: 2rem;
  }

  @media(max-width: 1024) { 
    padding: 0 1rem;
    margin-bottom: 2rem;
  }

  @media(max-width: 1280px) { 
    padding: 0 1rem;
    margin-bottom: 2rem;
  }
`;

export const InfoContent = styled.div`
padding: 1rem;
`;

export const DetailsContent = styled.div`
  background-color: ${(props) => props.color};
  padding: 1rem;
  border-radius: 2rem;
`;

export const DetailsImageContainer = styled.div`
  width: 100%;
`;

export const DetailImage = styled.img`
  border-radius: 1rem;
  width: 100%;
`;

export const StripescontainerLeft = styled.div``;

export const StripescontainerRight = styled.div`
  float: right;
  width: 90px;
`;

export const BlossomStripe = styled.div`
  position: fixed;
  margin-left: 30px;
  width: 30px;
  height: 99vh;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.blossomPink};
`;

export const BubblesStripe = styled.div`
  position: fixed;
  margin-left: 60px;
  width: 30px;
  height: 99vh;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.bubblesBlue};
`;

export const ButtercupStripe = styled.div`
  position: fixed;
  width: 30px;
  height: 99vh;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.buttercupGreen};
`;
