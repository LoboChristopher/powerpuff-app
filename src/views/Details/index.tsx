import React, { ReactElement, useCallback, useEffect, useState } from "react";
import IPage from "../../interfaces/page";
import { RouteComponentProps, Link } from "react-router-dom";
import Button from "@mui/material/Button";

import api from "../../services/api";

import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";

import { detailsActionCreator, State } from "../../state";
import light from "../../styles/themes/light";

import {
  Container,
  ContentContainer,
  DetailsContainer,
  DetailsContentContainer,
  DetailsContent,
  InfoContent,
  DetailsImageContainer,
  DetailImage,
  StripescontainerLeft,
  StripescontainerRight,
  BlossomStripe,
  BubblesStripe,
  ButtercupStripe,
} from "./styles";

const Details: React.FunctionComponent<IPage & RouteComponentProps<any>> = (
  props
) => {
  const episodeNumber = props.match.params.episodeId;
  let backgroundColor = props.location.hash;
  const dispatch = useDispatch();
  const { details } = bindActionCreators(detailsActionCreator, dispatch);
  const episode = useSelector((state: State) => state.details);
  const [renderDetails, setRenderDetails] = useState(false);
  const [buttonColor, setButtonColor] = useState(props.location.hash);

  async function getEpisodeDetails() {
    try {
      const { data } = await api.get("episodes/" + episodeNumber);
      details(data);
      setRenderDetails(true);
    } catch (error) {
      alert("There was a problem fetching the Episode");
    }
  }

  async function invertButtonColor() {
    console.log("Cor do botão antes: ", buttonColor);
    if (buttonColor === light.colors.blossomPink) {
      setButtonColor(light.colors.buttercupGreen);
    } else if (buttonColor === light.colors.bubblesBlue) {
      setButtonColor(light.colors.blossomPink);
    } else if (buttonColor === light.colors.buttercupGreen) {
      setButtonColor(light.colors.bubblesBlue);
    }
    console.log(props.location.hash);
    console.log("Cor do botão: ", buttonColor);
  }

  useEffect(() => {
    invertButtonColor();
    getEpisodeDetails();
  }, []);

  return (
    <Container>
      <StripescontainerLeft>
        <ButtercupStripe />
        <BlossomStripe />
        <BubblesStripe />
      </StripescontainerLeft>
      <ContentContainer>
        {renderDetails === true && (
          <DetailsContainer color={backgroundColor}>
            <DetailsImageContainer>
              <DetailImage src={episode.image.original} />
            </DetailsImageContainer>
            <DetailsContentContainer>
              <InfoContent>
                <p>Name: {episode.name}</p>
                <p>Season {episode.season}</p>
              </InfoContent>
              <DetailsContent color={buttonColor}>
                <p>
                  <div
                    dangerouslySetInnerHTML={{ __html: `${episode.summary}` }}
                  />
                </p>
              </DetailsContent>
            </DetailsContentContainer>
            <Button
              component={Link}
              to="/"
              sx={{
                backgroundColor: `${buttonColor}`,
                height: '5vh',
                width: '7rem'
              }}
              variant="contained"
            >
              Back
            </Button>
          </DetailsContainer>
        )}
      </ContentContainer>
      <StripescontainerRight>
        <ButtercupStripe />
        <BlossomStripe />
        <BubblesStripe />
      </StripescontainerRight>
    </Container>
  );
};

export default Details;
