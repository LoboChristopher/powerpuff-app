import {
  BrowserRouter,
  Route,
  Switch,
  RouteComponentProps,
} from "react-router-dom";
import { ThemeProvider } from "styled-components";

import lightTheme from "./styles/themes/light";
import routes from "./config/routes";

const App: React.FC<{}> = (props) => {
  return (
    <ThemeProvider theme={lightTheme}>
      <BrowserRouter>
        <Switch>
          {routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                render={(props: RouteComponentProps<any>) => (
                  <route.component
                    name={route.name}
                    {...props}
                    {...route.props}
                  />
                )}
              />
            );
          })}
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
