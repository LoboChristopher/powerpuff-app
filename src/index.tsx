import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import { store } from "./state/index";
import  GlobalStyle  from "./styles/global";
import lightTheme from "./styles/themes/light";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <GlobalStyle theme={lightTheme}/> 
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
